﻿namespace LivestreamConverter
{
    partial class frmMain
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtbChannel = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.dgvRecorders = new System.Windows.Forms.DataGridView();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbLoading = new System.Windows.Forms.PictureBox();
            this.btnStop = new System.Windows.Forms.PictureBox();
            this.btnStart = new System.Windows.Forms.PictureBox();
            this.pbAddJob = new System.Windows.Forms.PictureBox();
            this.btnDownloadM3U8 = new System.Windows.Forms.PictureBox();
            this.chbAutoStart = new MetroFramework.Controls.MetroCheckBox();
            this.colStatus = new System.Windows.Forms.DataGridViewImageColumn();
            this.colChannel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPlay = new System.Windows.Forms.DataGridViewImageColumn();
            this.Delete = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecorders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownloadM3U8)).BeginInit();
            this.SuspendLayout();
            // 
            // txtbChannel
            // 
            // 
            // 
            // 
            this.txtbChannel.CustomButton.Image = null;
            this.txtbChannel.CustomButton.Location = new System.Drawing.Point(114, 1);
            this.txtbChannel.CustomButton.Name = "";
            this.txtbChannel.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtbChannel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtbChannel.CustomButton.TabIndex = 1;
            this.txtbChannel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtbChannel.CustomButton.UseSelectable = true;
            this.txtbChannel.CustomButton.Visible = false;
            this.txtbChannel.Lines = new string[0];
            this.txtbChannel.Location = new System.Drawing.Point(23, 31);
            this.txtbChannel.MaxLength = 32767;
            this.txtbChannel.Name = "txtbChannel";
            this.txtbChannel.PasswordChar = '\0';
            this.txtbChannel.PromptText = "Channel Name";
            this.txtbChannel.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtbChannel.SelectedText = "";
            this.txtbChannel.SelectionLength = 0;
            this.txtbChannel.SelectionStart = 0;
            this.txtbChannel.ShortcutsEnabled = true;
            this.txtbChannel.Size = new System.Drawing.Size(136, 23);
            this.txtbChannel.TabIndex = 0;
            this.txtbChannel.UseSelectable = true;
            this.txtbChannel.WaterMark = "Channel Name";
            this.txtbChannel.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtbChannel.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtbChannel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtbChannel_KeyDown);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(55, 200);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(104, 19);
            this.metroLabel1.TabIndex = 7;
            this.metroLabel1.Text = "m3u8 download";
            // 
            // dgvRecorders
            // 
            this.dgvRecorders.AllowUserToAddRows = false;
            this.dgvRecorders.AllowUserToDeleteRows = false;
            this.dgvRecorders.AllowUserToResizeColumns = false;
            this.dgvRecorders.AllowUserToResizeRows = false;
            this.dgvRecorders.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvRecorders.ColumnHeadersHeight = 25;
            this.dgvRecorders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvRecorders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colStatus,
            this.colChannel,
            this.colSize,
            this.colLength,
            this.colPlay,
            this.Delete});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRecorders.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvRecorders.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvRecorders.Location = new System.Drawing.Point(227, 72);
            this.dgvRecorders.Name = "dgvRecorders";
            this.dgvRecorders.RowHeadersVisible = false;
            this.dgvRecorders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRecorders.Size = new System.Drawing.Size(493, 214);
            this.dgvRecorders.TabIndex = 14;
            this.dgvRecorders.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRecorders_CellClick);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(61, 260);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(109, 19);
            this.metroLabel2.TabIndex = 17;
            this.metroLabel2.Text = "Standard Ordner";
            this.metroLabel2.Click += new System.EventHandler(this.metroLabel2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::LivestreamConverter.Properties.Resources._interface;
            this.pictureBox1.Location = new System.Drawing.Point(23, 254);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pbLoading
            // 
            this.pbLoading.Image = global::LivestreamConverter.Properties.Resources.loading;
            this.pbLoading.Location = new System.Drawing.Point(195, 30);
            this.pbLoading.Name = "pbLoading";
            this.pbLoading.Size = new System.Drawing.Size(24, 24);
            this.pbLoading.TabIndex = 12;
            this.pbLoading.TabStop = false;
            this.pbLoading.Visible = false;
            // 
            // btnStop
            // 
            this.btnStop.Image = global::LivestreamConverter.Properties.Resources.stop_button;
            this.btnStop.Location = new System.Drawing.Point(688, 34);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(32, 32);
            this.btnStop.TabIndex = 11;
            this.btnStop.TabStop = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            this.btnStop.MouseEnter += new System.EventHandler(this.btnStop_MouseEnter);
            this.btnStop.MouseLeave += new System.EventHandler(this.btnStop_MouseLeave);
            // 
            // btnStart
            // 
            this.btnStart.Image = global::LivestreamConverter.Properties.Resources.pause;
            this.btnStart.Location = new System.Drawing.Point(650, 34);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(32, 32);
            this.btnStart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btnStart.TabIndex = 10;
            this.btnStart.TabStop = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            this.btnStart.MouseEnter += new System.EventHandler(this.btnStart_MouseEnter);
            this.btnStart.MouseLeave += new System.EventHandler(this.btnStart_MouseLeave);
            // 
            // pbAddJob
            // 
            this.pbAddJob.Image = ((System.Drawing.Image)(resources.GetObject("pbAddJob.Image")));
            this.pbAddJob.Location = new System.Drawing.Point(165, 30);
            this.pbAddJob.Name = "pbAddJob";
            this.pbAddJob.Size = new System.Drawing.Size(24, 24);
            this.pbAddJob.TabIndex = 9;
            this.pbAddJob.TabStop = false;
            this.pbAddJob.Click += new System.EventHandler(this.pbAddJob_Click);
            this.pbAddJob.MouseEnter += new System.EventHandler(this.pbAddJob_MouseEnter);
            this.pbAddJob.MouseLeave += new System.EventHandler(this.pbAddJob_MouseLeave);
            // 
            // btnDownloadM3U8
            // 
            this.btnDownloadM3U8.Image = global::LivestreamConverter.Properties.Resources.noun_Download_3247914;
            this.btnDownloadM3U8.Location = new System.Drawing.Point(23, 194);
            this.btnDownloadM3U8.Name = "btnDownloadM3U8";
            this.btnDownloadM3U8.Size = new System.Drawing.Size(32, 32);
            this.btnDownloadM3U8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDownloadM3U8.TabIndex = 6;
            this.btnDownloadM3U8.TabStop = false;
            this.btnDownloadM3U8.Click += new System.EventHandler(this.btnDownloadM3U8_Click);
            this.btnDownloadM3U8.MouseEnter += new System.EventHandler(this.btnDownloadM3U8_MouseEnter);
            this.btnDownloadM3U8.MouseLeave += new System.EventHandler(this.btnDownloadM3U8_MouseLeave);
            // 
            // chbAutoStart
            // 
            this.chbAutoStart.AutoSize = true;
            this.chbAutoStart.Location = new System.Drawing.Point(23, 60);
            this.chbAutoStart.Name = "chbAutoStart";
            this.chbAutoStart.Size = new System.Drawing.Size(102, 15);
            this.chbAutoStart.TabIndex = 18;
            this.chbAutoStart.Text = "Autom. starten";
            this.chbAutoStart.UseSelectable = true;
            // 
            // colStatus
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colStatus.DefaultCellStyle = dataGridViewCellStyle1;
            this.colStatus.HeaderText = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colStatus.Width = 50;
            // 
            // colChannel
            // 
            this.colChannel.HeaderText = "Channel";
            this.colChannel.Name = "colChannel";
            this.colChannel.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colChannel.Width = 120;
            // 
            // colSize
            // 
            this.colSize.HeaderText = "Size";
            this.colSize.Name = "colSize";
            this.colSize.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colSize.Width = 150;
            // 
            // colLength
            // 
            this.colLength.HeaderText = "Length";
            this.colLength.Name = "colLength";
            this.colLength.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colLength.Width = 90;
            // 
            // colPlay
            // 
            this.colPlay.HeaderText = "Play";
            this.colPlay.Name = "colPlay";
            this.colPlay.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colPlay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colPlay.Width = 40;
            // 
            // Delete
            // 
            this.Delete.HeaderText = "Delete";
            this.Delete.Name = "Delete";
            this.Delete.Width = 40;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 293);
            this.Controls.Add(this.chbAutoStart);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.dgvRecorders);
            this.Controls.Add(this.pbLoading);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.pbAddJob);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.btnDownloadM3U8);
            this.Controls.Add(this.txtbChannel);
            this.DisplayHeader = false;
            this.Name = "frmMain";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Orange;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Shown += new System.EventHandler(this.frmMain_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecorders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownloadM3U8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox txtbChannel;
        private System.Windows.Forms.PictureBox btnDownloadM3U8;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.PictureBox pbAddJob;
        private System.Windows.Forms.PictureBox btnStart;
        private System.Windows.Forms.PictureBox btnStop;
        private System.Windows.Forms.PictureBox pbLoading;
        private System.Windows.Forms.DataGridView dgvRecorders;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroCheckBox chbAutoStart;
        private System.Windows.Forms.DataGridViewImageColumn colStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colChannel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLength;
        private System.Windows.Forms.DataGridViewImageColumn colPlay;
        private System.Windows.Forms.DataGridViewImageColumn Delete;
    }
}

