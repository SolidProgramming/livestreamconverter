﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace LivestreamConverter
{
    public partial class frmMain : MetroForm
    {
        private static List<Recorder> recorders = new List<Recorder>();
        private static Dictionary<string, CancellationTokenSource> tokenSources = new Dictionary<string, CancellationTokenSource>();

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            Player.OnPlayerStopped += Player_OnPlayerStopped;
            Settings.DefaultFolder = Properties.Settings.Default.DefaultFolderPath;
        }

        private void Player_OnPlayerStopped(string channelName)
        {
            foreach (DataGridViewRow dataRow in dgvRecorders.Rows)
            {
                if (dataRow.Cells["colChannel"].Value.ToString() == channelName)
                {
                    dataRow.Cells["colPlay"].Value = Properties.Resources.playaudio;
                }
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            string channelName = GetSelectedChannel();

            var job = JobProvider.GetJobByChannel(channelName);

            if (job == null) { return; }

            var recorder = new Recorder(job);

            recorder.OnConverterProgressChanged += Recorder_OnConverterProgressChanged;
            recorder.OnRecorderStarted += Recorder_OnRecorderStarted;
            recorder.OnRecorderStopped += Recorder_OnRecorderStopped;

            recorders.Add(recorder);
            recorder.StartRecording(job.StreamUrl);
        }

        private void Recorder_OnRecorderStopped(string channelName, bool userStopped)
        {
            RecorderStoppedAction(channelName, userStopped);
        }

        private void RecorderStoppedAction(string channelName, bool userStopped)
        {
            foreach (DataGridViewRow dataRow in dgvRecorders.Rows)
            {
                if (dataRow.Cells["colChannel"].Value.ToString() == channelName)
                {
                    dataRow.Cells["colStatus"].Value = Properties.Resources.status_red;
                }
            }

            var recorder = recorders.SingleOrDefault(x => x != null && x.ChannelName == channelName);

            if (userStopped)
            {
                if (recorder != null)
                {
                    recorders.Remove(recorder);
                }
            }          

        }

        private void Recorder_OnRecorderStarted(string channelName, Recorder recorder)
        {

            foreach (DataGridViewRow dataRow in dgvRecorders.Rows)
            {
                if (dataRow.Cells["colChannel"].Value.ToString() == channelName)
                {
                    dataRow.Cells["colStatus"].Value = Properties.Resources.status_yellow;
                }
            }

        }

        private void Recorder_OnConverterProgressChanged(ConvertProgress progress, string channelName)
        {

            foreach (DataGridViewRow dataRow in dgvRecorders.Rows)
            {
                if (dataRow.Cells["colChannel"].Value.ToString() == channelName)
                {
                    if (progress.Size >= 1024)
                    {
                        //TODO: stringbuilder
                        dataRow.Cells["colSize"].Value = Math.Round(progress.Size / 1024f, 2).ToString() + " mb";
                    }
                    else
                    {
                        dataRow.Cells["colSize"].Value = progress.Size.ToString() + " kB";
                    }

                    if (progress.FPS > 0)
                    {
                        dataRow.Cells["colSize"].Value += " @" + progress.FPS + " FPS";
                    }

                    dataRow.Cells["colLength"].Value = progress.Time;
                }
            }

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            var channelName = GetSelectedChannel();

            var recorder = recorders.SingleOrDefault(x => x != null && x.ChannelName == channelName);

            RecordJob recordJob = JobProvider.GetJobByChannel(channelName);

            if (recordJob.AutoStartRecording)
            {
                var result = MessageBox.Show("Autostart wird gestoppt/nWirklich stoppen?", "Autostart", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    if (tokenSources.ContainsKey(channelName))
                    {
                        CancellationTokenSource tokenSource = tokenSources[channelName];

                        tokenSource.Cancel();
                        tokenSource.Dispose();
                        tokenSources.Remove(channelName);

                        foreach (DataGridViewRow dataRow in dgvRecorders.Rows)
                        {
                            if (dataRow.Cells["colChannel"].Value.ToString() == channelName)
                            {
                                dataRow.Cells["colStatus"].Value = Properties.Resources.status_red;
                            }
                        }
                    }
                }
            }

            if (recorder != null)
            {
                recorder.Abort();
            }
        }

        private void btnDownloadM3U8_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtbChannel.Text))
            {
                MessageBox.Show("Keinen Channel ausgewählt!");
                return;
            }

            var folderDialog = new FolderBrowserDialog();

            if (folderDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            using (var wc = new WebClient())
            {
                string streamUrl;

                if (StreamExtractor.TryFindStreamUrl(txtbChannel.Text, out streamUrl))
                {
                    wc.DownloadFile(streamUrl, folderDialog.SelectedPath + "\\" + txtbChannel.Text + ".m3u8");
                }
                else
                {
                    MessageBox.Show("Channel oder stream url nicht gefunden!");
                }
            }
        }

        private void btnDownloadM3U8_MouseEnter(object sender, EventArgs e)
        {
            btnDownloadM3U8.BackColor = Color.LightGray;
        }

        private void btnDownloadM3U8_MouseLeave(object sender, EventArgs e)
        {
            btnDownloadM3U8.BackColor = Color.White;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            Settings.DefaultFolder = Properties.Settings.Default.DefaultFolderPath;
        }

        private void pbAddJob_MouseEnter(object sender, EventArgs e)
        {
            pbAddJob.Image = Properties.Resources.add2;
        }

        private void pbAddJob_MouseLeave(object sender, EventArgs e)
        {
            pbAddJob.Image = Properties.Resources.add;
        }

        private void btnStart_MouseEnter(object sender, EventArgs e)
        {
            btnStart.Image = Properties.Resources.pause2;
        }

        private void btnStart_MouseLeave(object sender, EventArgs e)
        {
            btnStart.Image = Properties.Resources.pause;
        }

        private void btnStop_MouseEnter(object sender, EventArgs e)
        {
            btnStop.Image = Properties.Resources.stop_button2;
        }

        private void btnStop_MouseLeave(object sender, EventArgs e)
        {
            btnStop.Image = Properties.Resources.stop_button;
        }

        private void pbAddJob_Click(object sender, EventArgs e)
        {
            bool autostart = chbAutoStart.Checked;

            if (autostart)
            {
                AddAutostartJob();
            }
            else
            {
                AddJob();
            }            
        }

        private void txtbChannel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == (char)Keys.Enter)
            {
                AddJob();
            }
        }

        private async void AddJob()
        {
            if (JobProvider.GetJobByChannel(txtbChannel.Text) != null)
            {
                MessageBox.Show("Du kannst den Channel nur einmal in die List aufnehmen!");
                return;
            }

            bool success = false;
            pbLoading.Visible = true;            

            string streamUrl = "";

            var result = MessageBox.Show("Soll auch Video aufgenommen werden?", "Video?", MessageBoxButtons.YesNo);

            bool captureVideo = false;
            bool autoStart = chbAutoStart.Checked;

            if (result == DialogResult.Yes)
            {
                captureVideo = true;
            }
            await Task.Factory.StartNew(() =>
            {
                success = StreamExtractor.TryFindStreamUrl(txtbChannel.Text, out streamUrl, captureVideo, autoStart);
            });


            pbLoading.Visible = false;

            if (!success && !chbAutoStart.Checked)
            {
                MessageBox.Show("Channel oder stream url nicht gefunden!");
                return;
            }

            string folderPath = FolderDialog();

            ListViewItem lvItem = new ListViewItem();

            lvItem.ImageKey = Status.observing.ToString();
            lvItem.SubItems.Add(txtbChannel.Text.Trim());
            lvItem.SubItems.Add("0kB");
            lvItem.SubItems.Add("00:00:00");

            dgvRecorders.Rows.Add(Properties.Resources.status_green, txtbChannel.Text, "0kB", "00:00:00", Properties.Resources.playaudio, Properties.Resources.delete);

            //var filename = Path.Combine(folderPath, txtbChannel.Text, $"{txtbChannel.Text}_{DateTime.Now.Hour}h{DateTime.Now.Minute}m{DateTime.Now.Second}s.ogg");
            JobProvider.AddJob(new RecordJob() { ConvertProgress = new ConvertProgress(), StreamUrl = streamUrl, ChannelName = txtbChannel.Text, FolderPath = folderPath, CaptureVideo = captureVideo, AutoStartRecording = autoStart });

        }
                
        //TODO: refactor
        private void AddAutostartJob()
        {
            if (JobProvider.GetJobByChannel(txtbChannel.Text) != null)
            {
                MessageBox.Show("Du kannst den Channel nur einmal in die List aufnehmen!");
                return;
            }
            
            bool captureVideo = false;

            var result = MessageBox.Show("Soll auch Video aufgenommen werden?", "Video?", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                captureVideo = true;
            }

            string folderPath = FolderDialog();

            ListViewItem lvItem = new ListViewItem();

            lvItem.ImageKey = Status.observing.ToString();
            lvItem.SubItems.Add(txtbChannel.Text.Trim());
            lvItem.SubItems.Add("0kB");
            lvItem.SubItems.Add("00:00:00");

            dgvRecorders.Rows.Add(Properties.Resources.status_yellow, txtbChannel.Text, "0kB", "00:00:00", Properties.Resources.playaudio, Properties.Resources.delete);

            JobProvider.AddJob(new RecordJob() { ConvertProgress = new ConvertProgress(), ChannelName = txtbChannel.Text, FolderPath = folderPath, CaptureVideo = captureVideo, AutoStartRecording = true });

            StartWaitStreamer(txtbChannel.Text);
        }

        private string FolderDialog()
        {
            if (string.IsNullOrEmpty(Settings.DefaultFolder))
            {
                var saveFilenameDialog = new FolderBrowserDialog();

                if (saveFilenameDialog.ShowDialog() != DialogResult.OK)
                {
                    return null;
                }

                return saveFilenameDialog.SelectedPath;
            }
            else
            {
                return Settings.DefaultFolder;
            }
        }

        private async void StartWaitStreamer(string channelName)
        {
            RecordJob job = JobProvider.GetJobByChannel(channelName);

            //TODO: memory leak?
            #region
            CancellationTokenSource tokenSource = new CancellationTokenSource();

            tokenSources.Add(channelName, tokenSource);

            CancellationToken cancellationToken = tokenSource.Token;
            #endregion

            string streamUrl = await StreamExtractor.GetStreamUrlAsync(job.ChannelName, cancellationToken, job.CaptureVideo);

            job.StreamUrl = streamUrl;

            var recorder = new Recorder(job);

            recorder.OnConverterProgressChanged += Recorder_OnConverterProgressChanged;
            recorder.OnRecorderStarted += Recorder_OnRecorderStarted;
            recorder.OnRecorderStopped += Recorder_OnRecorderStopped;

            recorders.Add(recorder);
            recorder.StartRecording(job.StreamUrl);
        }

        private void Recorder_OnRecorderStreamOnline(string channelName)
        {
            var recorder = recorders.SingleOrDefault(_ => _.ChannelName == channelName);
            var job = JobProvider.GetJobByChannel(channelName);
            recorder.StartRecording(job.StreamUrl);
        }

        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == (char)Keys.Enter)
            {
                AddJob();
            }
        }

        private string GetSelectedChannel()
        {
            if (dgvRecorders.SelectedRows.Count > 0)
            {
                return dgvRecorders.Rows[dgvRecorders.SelectedRows[0].Index].Cells["colChannel"].Value.ToString();
            }

            return null;
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (recorders.Count(x => x.Running) > 0)
            {
                var result = MessageBox.Show("Wenn du dieses Fenster schließ werden alle Recordings abgebrochen\nTrotzdem schließen?", "Wirklich?", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    StopAllRecordings();
                }
                else
                {
                    e.Cancel = true;
                }
            }

            if (Player.IsPlaying)
            {
                Player.Stop();
            }

        }

        private void dgvRecorders_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var colName = dgvRecorders.Columns[dgvRecorders.CurrentCell.ColumnIndex].Name;

            if (colName != "colPlay" && colName != "Delete") return;

            if (colName == "colPlay")
            {
                PlayAktion();
            }
            else if (colName == "Delete")
            {
                var rowIndex = dgvRecorders.CurrentCell.RowIndex;
                var channel = GetSelectedChannel();
                JobProvider.Delete(channel);
                DeleteAktion(rowIndex, channel);
            }
        }

        private void PlayAktion()
        {
            dgvRecorders.CurrentCell.Value = Properties.Resources.pauseplayer;
            var channelName = GetSelectedChannel();

            if (channelName == Player.ChannelName)
            {
                Player.Stop();
                return;
            }

            Player.Stop();

            System.Threading.Thread.Sleep(500);

            Player.PlayChannel(JobProvider.GetJobByChannel(channelName));
        }

        private void DeleteAktion(int rowIndex, string channel)
        {
            if (MessageBox.Show("Wirklich löschen?", "Löschen", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var recorder = recorders.SingleOrDefault(x => x.ChannelName == channel);

                if (recorder != null)
                {
                    recorder.Abort();
                }

                JobProvider.GetJobByChannel(channel).Delete();

                dgvRecorders.Rows.RemoveAt(rowIndex);
            }
        }

        private void StopAllRecordings()
        {
            for (int i = 0; i < recorders.Count; i++)
            {
                recorders[i].Abort();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            DefaultFolderDialog();
        }

        private void DefaultFolderDialog()
        {
            var saveFilenameDialog = new FolderBrowserDialog();

            if (saveFilenameDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            Settings.DefaultFolder = saveFilenameDialog.SelectedPath;

            Properties.Settings.Default.DefaultFolderPath = Settings.DefaultFolder;
            Properties.Settings.Default.Save();
        }

        private void metroLabel2_Click(object sender, EventArgs e)
        {
            DefaultFolderDialog();
        }

    }
}
