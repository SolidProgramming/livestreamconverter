﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace LivestreamConverter
{
    public static class StreamExtractor
    {
        private static Process process;

        public static bool TryFindStreamUrl(string channelName, out string streamUrl, bool captureVideo = false, bool autoStart = false)
        {
            process = new Process();

            process.StartInfo.FileName = "streamlink";
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = false;

            string[] args;
            if (channelName.Contains("facebook.com"))
            {
                args = new string[] { "--stream-url", channelName, "best", "--stdout" };
            }
            else
            {
                if (captureVideo)
                {
                    args = new string[] { "--stream-url", "twitch.tv/" + channelName, "best", "--stdout", autoStart ? "--retry-streams 10" : "" };
                }
                else
                {
                    args = new string[] { "--stream-url", "twitch.tv/" + channelName, "audio_only", "--stdout", autoStart ? "--retry-streams 10" : "" };
                }
            }

            string arguments = string.Join(" ", args);
            process.StartInfo.Arguments = arguments;

            process.Start();

            StreamReader reader = process.StandardError;
            string output = reader.ReadToEnd().Replace("\r", "").Replace("\n", "");

            process.Close();
            process.Dispose();

            if (output.StartsWith("error"))
            {
                streamUrl = null;
                return false;
            }

            streamUrl = output;

            return true;
        }

        public static async Task<string> GetStreamUrlAsync(string channelName, CancellationToken cancellationToken, bool captureVideo = false)
        {
            process = new Process();

            process.StartInfo.FileName = "streamlink";
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = false;

            string[] args;
            if (channelName.Contains("facebook.com"))
            {
                args = new string[] { "--stream-url", channelName, "best", "--stdout" };
            }
            else
            {
                if (captureVideo)
                {
                    args = new string[] { "--stream-url", "twitch.tv/" + channelName, "best", "--stdout", "--retry-streams 10" };
                }
                else
                {
                    args = new string[] { "--stream-url", "twitch.tv/" + channelName, "audio_only", "--stdout", "--retry-streams 10" };
                }
            }

            string arguments = string.Join(" ", args);
            process.StartInfo.Arguments = arguments;

            string output = "";

            //Long running
            await Task.Run(() =>
            {
                process.Start();

                StreamReader reader = process.StandardError;
                output = reader.ReadToEnd().Replace("\r", "").Replace("\n", "");

                process.Close();
                process.Dispose();

            }, cancellationToken);

            if (cancellationToken.IsCancellationRequested)
            {
                return null;
            }

            if (string.IsNullOrEmpty(output) ||output.StartsWith("error"))
            {                
                return null;
            }

            return output;
        }

    }
}
