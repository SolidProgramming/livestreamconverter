﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LivestreamConverter
{
    public class Recorder
    {
        public delegate void OnRecorderProgressChangedEvent(ConvertProgress progress, string channelName);
        public event OnRecorderProgressChangedEvent OnConverterProgressChanged;

        public delegate void OnRecorderStartedEvent(string channelName, Recorder recorder);
        public event OnRecorderStartedEvent OnRecorderStarted;

        public delegate void OnRecorderStoppedEvent(string channelName, bool userStopped);
        public event OnRecorderStoppedEvent OnRecorderStopped;

        string args;
        Process process;
        public string ChannelName;
        public bool Running = false;
        public bool UserStopped = false;
        public string OutputFile;
        private int Runtime = -1;
        private System.Timers.Timer timer;

        public Recorder(RecordJob recordJob)
        {
            var filename = Path.Combine(recordJob.FolderPath, recordJob.ChannelName, DateTime.Today.Date.ToString("dd/MM/yyyy"), $"{recordJob.ChannelName}_{recordJob.StartTime}");

            if (recordJob.CaptureVideo)
            {
                filename += ".ts";
            }
            else
            {
                filename += ".ogg";
            }

            var directory = Path.GetDirectoryName(filename);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            OutputFile = filename;

            if (recordJob.CaptureVideo)
            {
                args = "-y -i $M3U8_URL -vcodec copy " + "\"" + filename + "\"";
            }
            else
            {
                args = "-y -i $M3U8_URL -f ogg -c:a flac " + "\"" + filename + "\"";
            }

            Runtime = recordJob.Runtime;
            ChannelName = recordJob.ChannelName;
        }

        public void StartRecording(string m3u8_url)
        {

            Running = true;
            OnRecorderStarted(this.ChannelName, this);
            process = new Process();

            process.StartInfo.FileName = Directory.GetCurrentDirectory() + "\\ffmpeg.exe";
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.StandardErrorEncoding = Encoding.UTF8;
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.EnableRaisingEvents = true;
            process.StartInfo.CreateNoWindow = true;

            process.StartInfo.UseShellExecute = false;

            process.StartInfo.StandardOutputEncoding = Encoding.UTF8;

            process.StartInfo.Arguments = args.Replace("$M3U8_URL", m3u8_url);

            process.OutputDataReceived += Process_OutputDataReceived;
            process.Exited += Process_Exited;

            process.Start();

            new Task(ReadOutputErrorAsync).Start();
            new Task(ReadOutputAsync).Start();

            if (Runtime != -1 && Runtime > 0)
            {
                timer = new System.Timers.Timer();
                timer.Interval = Runtime;
                timer.Elapsed += Timer_Elapsed;
                timer.AutoReset = false;
                timer.Enabled = true;
#if DEBUG
                Console.WriteLine($"Timer started for {Runtime}ms");
#endif
            }

        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Abort();
        }

        private async void ReadOutputErrorAsync()
        {
            var sb = new StringBuilder();
            var buff = new char[1024];
            int length;

            while (process.HasExited == false)
            {
                sb.Clear();

                length = await process.StandardOutput.ReadAsync(buff, 0, buff.Length);
                sb.Append(SubArray(buff, 0, length));
                Console.WriteLine(sb.ToString());
                Thread.Sleep(10);
            }

        }

        private async void ReadOutputAsync()
        {
            var sb = new StringBuilder();

            do
            {
                sb.Clear();

                var buff = new char[1024];

                int length = await process.StandardError.ReadAsync(buff, 0, buff.Length);

                sb.Append(SubArray(buff, 0, length));
#if DEBUG
                var output = sb.ToString();

                Console.WriteLine(output);
#endif
                if (!Regex.IsMatch(output, "time=(.*?) bitrate"))
                {
                    Thread.Sleep(50);

                    continue;
                }

                var progress = new ConvertProgress();

                //TODO: handle gracefully
                try
                {
                    progress.Size = float.Parse(Regex.Match(output, "size=.*?(\\d+\\.\\d+|\\d+)kB time").Groups[1].Value);
                    progress.Time = Regex.Match(output, "time=(.*?) bitrate").Groups[1].Value;
                    progress.Bitrate = float.Parse(Regex.Match(output, "bitrate=.*?(\\d+\\.\\d+|\\d+)kbits").Groups[1].Value);
                    progress.Speed = float.Parse(Regex.Match(output, "speed=.*?(\\d+\\.\\d+|\\d+)").Groups[1].Value);

                    //TODO: handle gracefully
                    try
                    {
                        progress.FPS = float.Parse(Regex.Match(output, "fps=.*?(\\d+\\.\\d+|\\d+)").Groups[1].Value);                        
                    }
                    catch (Exception) { }
                }
                catch (Exception) { }

                OnConverterProgressChanged(progress, this.ChannelName);
                Thread.Sleep(1);
            }

            while (process.HasExited == false);
        }

        private char[] SubArray(char[] input, int startIndex, int length)
        {
            List<char> result = new List<char>();
            for (int i = startIndex; i < length; i++)
            {
                result.Add(input[i]);
            }

            return result.ToArray();
        }

        private void Process_Exited(object sender, EventArgs e)
        {
            OnRecorderStopped(this.ChannelName, UserStopped);
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {

        }

        public void Abort()
        {
            try
            {
                if (process != null && process.HasExited == false)
                {
                    Running = false;
                    UserStopped = true;
                    if (this.timer != null)
                    {
                        this.timer.Enabled = false;
                    }

                    process.Kill();
                    OnRecorderStopped(this.ChannelName, UserStopped);

                }
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Process not associated. No need to kill");
            }
        }

    }
}
