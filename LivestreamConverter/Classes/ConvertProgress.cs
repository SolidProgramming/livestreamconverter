﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivestreamConverter
{
    public struct ConvertProgress
    {
        public float Size;
        public string Time;
        public float Bitrate;
        public float Speed;
        public float FPS;
    }
}
