﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LivestreamConverter
{
    public static class Player
    {
        
        private static Process process;
        public delegate void OnPlayerStoppedEvent(string channelName);
        public static event OnPlayerStoppedEvent OnPlayerStopped;

        public static bool IsPlaying = false;
        public static string ChannelName;

        public static void PlayChannel(RecordJob recordJob)
        {    
            
            IsPlaying = true;
            ChannelName = recordJob.ChannelName;

            process = new Process();

            process.StartInfo.FileName = Directory.GetCurrentDirectory() + "\\ffplay.exe";
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.StandardErrorEncoding = Encoding.UTF8;
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.EnableRaisingEvents = true;
            process.StartInfo.CreateNoWindow = true;

            process.StartInfo.UseShellExecute = false;

            process.StartInfo.StandardOutputEncoding = Encoding.UTF8;

            process.StartInfo.Arguments = recordJob.StreamUrl + " -autoexit -nodisp";

            //process.OutputDataReceived += Process_OutputDataReceived;
            process.Exited += Process_Exited;
            
            process.Start();

            new Task(ReadOutputAsync).Start();
            //new Task(ReadOutputErrorAsync).Start();
        }

        //private static async void ReadOutputErrorAsync()
        //{
            
        //}

        private static async void ReadOutputAsync()
        {
            var sb = new StringBuilder();

            do
            {
                sb.Clear();

                var buff = new char[1024];

                int length = await process.StandardError.ReadAsync(buff, 0, buff.Length);

                sb.Append(SubArray(buff, 0, length));

                var output = sb.ToString();

                Thread.Sleep(1);
            }

            while (process.HasExited == false);
        }

        private static char[] SubArray(char[] input, int startIndex, int length)
        {
            List<char> result = new List<char>();
            for (int i = startIndex; i < length; i++)
            {
                result.Add(input[i]);
            }

            return result.ToArray();
        }

        private static void Process_Exited(object sender, EventArgs e)
        {
            OnPlayerStopped(ChannelName);
            ChannelName = null;
        }

        private static void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
           
        }

        public static void Stop()
        {
            try
            {
                if (process != null && process.HasExited == false)
                {
                    process.Kill();
                    OnPlayerStopped(ChannelName);
                    IsPlaying = false;
                    ChannelName = null;
                }
            }
            catch (InvalidOperationException)
            {
                System.Windows.Forms.MessageBox.Show("Test");
                Console.WriteLine("Process not associated. Not need to kill");
            }
            catch (Exception)
            {
                System.Windows.Forms.MessageBox.Show("Test2");
            }
        }
    }
}
