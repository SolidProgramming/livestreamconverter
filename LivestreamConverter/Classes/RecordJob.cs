﻿using System;

namespace LivestreamConverter
{
    public class RecordJob
    {
        public string StreamUrl;
        public ConvertProgress ConvertProgress;
        public string ChannelName;        
        public string FolderPath;
        public bool CaptureVideo = false;
        public readonly string StartTime = $"{DateTime.Now.Hour}h{DateTime.Now.Minute}m{DateTime.Now.Second}s";
        public int Runtime = -1;
        public bool AutoStartRecording = false;
    }
}