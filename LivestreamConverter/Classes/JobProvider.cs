﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivestreamConverter
{
    public static class JobProvider
    {
        private static List<RecordJob> jobs = new List<RecordJob>();

        public static void AddJob(RecordJob recordJob)
        {
            if (!jobs.Contains(recordJob))
            {
                jobs.Add(recordJob);
            }
        }
        private static void Recorder_OnRecorderStreamOnline(string channelName)
        {
            var job = jobs.SingleOrDefault(_ => _.ChannelName == channelName);

            
        }

        public static RecordJob GetJobByChannel(string channelName)
        {
            return jobs.SingleOrDefault(x => x.ChannelName == channelName);
        }

        public static void Delete(this RecordJob recordJob)
        {
            jobs.Remove(recordJob);
        }

        public static void Delete(string channelName)
        {
            Delete(jobs.SingleOrDefault(x => x.ChannelName == channelName));
        }

    }
}
